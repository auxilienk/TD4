const xhr = new XMLHttpRequest();
const div_verbes = document.getElementById("liste_verbes");
const div_input = document.getElementById("input");

function creer_interface() {


    let rechercheElement = document.createElement("input");
    rechercheElement.id = "recherche";
    rechercheElement.type = "text";
    rechercheElement.placeholder = "entrez une séquence";
    div_input.insertAdjacentElement("afterbegin",rechercheElement);

    for (let i = 97; i < 123; i++) {
      let char = String.fromCharCode(i);
      let lettreElement = document.createElement("input");
      lettreElement.type="button";
      lettreElement.classList.add("letter");
      lettreElement.value=char;
      //insert = insert + "<input type=\"button\" class=\"letter\" value=" + char + " onclick=\"charger_verbes('"+char+"','init')\" >";
      div_input.insertAdjacentElement("beforeend",lettreElement);
    }
  let lettreElement = document.createElement("input");
  lettreElement.type="button";
  lettreElement.classList.add("letter");
  lettreElement.value="œ";
  div_input.insertAdjacentElement("beforeend",lettreElement);

  let effacerElement = document.createElement("input");
  effacerElement.type = "button";
  effacerElement.id="effacer";
  effacerElement.classList.add("erase");
  effacerElement.value="effacer la liste";
  div_input.insertAdjacentElement("beforeend",effacerElement);

  div_input.addEventListener('click',function (){
    if(event.target.classList.contains("letter")){
      charger_verbes(event.target.value,'init');
    }
  })

  let recherche = document.getElementById("recherche");
  recherche.addEventListener('input', function(){
      charger_verbes(event.target.value,"seq");

  });

  let effacer = document.getElementById('effacer');
  effacer.addEventListener('click',function (){
    document.getElementById("recherche").value = "";
    div_verbes.innerHTML ="";
  })
}

function callback_basique() {
  let xhrJSON = JSON.parse(xhr.responseText);
  console.log(xhrJSON);
}

function callback() {
    let xhrJSON = JSON.parse(xhr.responseText);
    div_verbes.innerHTML = "";
  for (let i = 0; i < xhrJSON.length; i++) {
    let p = document.createElement('p');
    console.log(xhrJSON[i].libelle);

    p.textContent = xhrJSON[i].libelle;
    div_verbes.append(p);
  }
}

/**
 * 
 * @param {String} lettre Chaîne recherchée dans les verbes
 * @param {String} type Type de recherche ('seq' ou 'init')
 */
function charger_verbes(lettre,type) {
  lettre = encodeURIComponent(lettre);
  type = encodeURIComponent(type);

  const formData = new FormData();
  formData.append("lettre", lettre);
  formData.append("type", type);

  let url = "https://webinfo.iutmontp.univ-montp2.fr/~auxilienk/JS/TD4/src/ex5/php/recherche.php";
  xhr.open("POST",url,true);

  xhr.addEventListener("load",function() {
    callback();
  });

  xhr.send(formData);

}


// Création de l'interface 
creer_interface();